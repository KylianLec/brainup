import QtQuick 2.15
import QtQuick.Controls 2.5
import SerieTableModel 1.0
import Serie 1.0

Page {  
    width: 700
    height: 420
    title: qsTr("Home")

    Label {
        x: rectangle.x + ((rectangle1.x - rectangle.x) / 2)  - (width / 2)
        y: 5
        text: qsTr("Brainup")
        font.pointSize: 30

    }

    Button {
        id: btnStart
        x: rectangle.x + 50
        y: 332
        width: 100
        height: 45
        enabled: false
        text: qsTr("Commencer")
        font.capitalization: Font.Capitalize
        onClicked: {
            listView.model.currSerie.initCurrentList(spbNbWord.value);
            stackView.push("LearnPage.qml")
        }
    }

    Button {
        id: btnQuit
        x: rectangle1.x - 50 - width
        y: 332
        width: 100
        height: 45
        text: qsTr("Quitter")
        font.capitalization: Font.Capitalize
        onClicked: {
            Qt.quit()
        }
    }

    Label {
        x: rectangle.x + ((rectangle1.x - rectangle.x) / 2)  - (width / 2)
        y: 167
        text: qsTr("Nombre de mots")
        font.pointSize: 16
    }

    SpinBox {
        id: spbNbWord
        x: rectangle.x + ((rectangle1.x - rectangle.x) / 2)  - (width / 2)
        y: 225
        enabled: false
        onValueModified: {
            if (spbNbWord.value > 0 && spbNbWord.value < listView.model.currSerie.initSize)
                btnStart.enabled = true
            else
                btnStart.enabled = false
        }
    }

    ButtonGroup { id: rGrpMode }

    RadioButton {
        id: rbtnFr
        x: 580
        y: 117
        height: 30
        text: "Fr -> En"
        font.pointSize: 10
        ButtonGroup.group: rGrpMode
        onClicked: listView.model.currSerie.mode = Serie.FrEn;
    }

    RadioButton {
        id: rbtnEn
        x: 580
        y: 153
        height: 30
        text: qsTr("En -> Fr")
        checked: true
        font.pointSize: 10
        ButtonGroup.group: rGrpMode
        onClicked: listView.model.currSerie.mode = Serie.EnFr;
    }

    RadioButton {
        id: rbtnRand
        x: 580
        y: 195
        height: 30
        text: qsTr("au hasard")
        font.pointSize: 10
        ButtonGroup.group: rGrpMode
        onClicked: listView.model.currSerie.mode = Serie.Random;
    }

    Component {
        id: seriesDelegate
        Item {
            id: itemDelegate
            height: 40
            width: 212
            Rectangle {
                id: rectHightlight
                anchors.fill: parent
                color: "#CBE4FE"
                visible: false
            }

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    if (listView.currentIndex != index)
                    {
                        listView.currentIndex = index
                        listView.model.setCurrSerie(listView.currentIndex)
                    }
                    spbNbWord.enabled = listView.currentIndex != -1 ? true : false
                }

                onEntered: {
                    rectHightlight.visible = true
                }

                onExited: {
                    rectHightlight.visible = false
                }
            }

            Row {
                id: row
                width: listView.width
                height: 40
                Text {
                    id: serieName
                    text: model.name
                    anchors.verticalCenter: parent.verticalCenter
                    font.bold: true
                }
                spacing: 10
            }
        }
    }

    ListView {
        id: listView
        x: 5
        y: 5
        width: 212
        height: parent.height - 10 - btnEditSeries.height
        leftMargin: 0
        rightMargin: 0
        bottomMargin: 0
        topMargin: 0
        delegate: seriesDelegate
        model: serieListModel
        currentIndex: -1
        clip: true
        highlight: Rectangle
        {
             color: "#72CCFE"
             opacity: 0.7
             focus: true
             Behavior on y {
                 SpringAnimation {
                     spring: 3
                     damping: 0.2
                 }
             }
        }
    }

    Button {
        id: btnEditSeries
        x: 5
        y: parent.height - height - 5
        width: 212
        height: 35
        text: qsTr("Modifier")
        font.capitalization: Font.Capitalize
        onClicked: {
            stackView.push("SeriesEdition.qml")
        }
    }

    Rectangle {
        id: rectangle
        x: 227
        y: 5
        width: 1
        height: parent.height - 10
        color: "#c8c8c8"
    }

    Rectangle {
        id: rectangle1
        x: 561
        y: 5
        width: 1
        height: parent.height - 10
        color: "#c8c8c8"
    }

    /*RadioButton {
        id: rbtnDouble
        x: 386
        y: 204
        height: 30
        text: qsTr("double")
        layer.textureSize.width: 0
        font.pointSize: 10
        ButtonGroup.group: rGrpMode
        onClicked: listView.model.currSerie.mode = listView.model.currSerie.Double
    }*/
}
