#ifndef SERIELISTMODEL_H
#define SERIELISTMODEL_H

#include <QAbstractListModel>
#include <QObject>
#include <QVector>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <iostream>

#include "Serie.h"

class SerieListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(Serie* currSerie READ currSerie NOTIFY sigCurrSerieChanged)
public:
    SerieListModel(QObject *parent = nullptr);

    enum {
        NameRole = Qt::UserRole,
    };

    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE bool append();
    Q_INVOKABLE bool remove(unsigned int index);
    Q_INVOKABLE void loadSeries(const QString &fileName = "res/series.json");
    Q_INVOKABLE void saveSeries();

    inline Serie* currSerie() const { return m_currSerie; }
    Q_INVOKABLE inline unsigned int size() const { return m_series.size(); }

    Q_INVOKABLE void setCurrSerie(int i = -1);

private:
    QVector<Serie*> m_series;
    Serie *m_currSerie = nullptr;

signals:
    void sigCurrSerieChanged();

};

#endif // SERIELISTMODEL_H
