import QtQuick 2.12
import QtQuick.Controls 2.5

Page
{
    width: 700
    height: 420
    title: qsTr("EndPage")

    Label
    {
        x: (parent.width/2) - (this.width/2)
        y: 60
        text: "Score :"
        font.pointSize: 30
    }

    Label
    {
        id: laScore
        x: (parent.width/2) - (this.width/2)
        y: 140
        text: serieListModel.currSerie.score + " / " + serieListModel.currSerie.shuffleSize
        font.pointSize: 20
    }

    Button
    {
        id: btnStart
        x: 210
        y: 268
        width: 100
        height: 45
        text: qsTr("Recommencer")
        font.capitalization: Font.Capitalize
        onClicked:
        {
            stackView.pop()
            stackView.pop()
        }
    }

    Button
    {
        id: btnQuit
        x: 389
        y: 268
        width: 100
        height: 45
        text: qsTr("Quitter")
        font.capitalization: Font.Capitalize
        onClicked: Qt.quit()
    }
}
