#include "SerieTableModel.h"

SerieTableModel::SerieTableModel()
{

}

int SerieTableModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    if (!m_serie) return 0;
    return m_serie->initSize();
}

int SerieTableModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 3;
}

QVariant SerieTableModel::data(const QModelIndex &index, int role) const
{
    if (index.isValid())
    {
        const int row = index.row();
        const int column = index.column();

        if (role == Qt::DisplayRole)
        {
            // Header
            if (row == 0)
            {
                switch(column)
                {
                case 0: return "Anglais";
                case 1: return "Français";
                }
            }

            // Body
            switch(column)
            {
            case 0:
                return m_serie->initWord(row).first;
            case 1:
                return m_serie->initWord(row).second;
            default: return "";
            }
        }
    }

    return QVariant();
}

bool SerieTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    const int row = index.row();
    const int column = index.column();
    if (index.isValid()/* && role == Qt::EditRole*/ && column <= 3 && column >= 0)
    {
        QPair<QString, QString>& p = m_serie->initWord(row);

        switch (column)
        {
            case 0: p.first = value.toString(); break;
            case 1: p.second = value.toString(); break;
            default: break;
        }

        emit(dataChanged(index, index));

        return true;
    }

    return false;
}

QVariant SerieTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal) {
        switch (section) {
            case 0:
                return tr("Anglais");
            case 1:
                return tr("Français");
            case 2:
                return tr("Supprimer");
            default:
                return QVariant();
        }
    }
    return QVariant();
}

QHash<int, QByteArray> SerieTableModel::roleNames() const
{
    return { {Qt::DisplayRole, "display"} };
}

bool SerieTableModel::append()
{
    if (m_serie)
    {
        beginInsertRows(QModelIndex(), m_serie->initSize(), m_serie->initSize());

        QPair<QString, QString> nPair;
        nPair.first = "New";
        nPair.second = "Nouveau";
        m_serie->append(nPair);
        endInsertRows();
        return true;
    }
    return false;
}


Qt::ItemFlags SerieTableModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
}

bool SerieTableModel::remove(unsigned int index)
{
    if (m_serie)
    {
        beginRemoveRows(QModelIndex(), index, index);

        m_serie->removeAt(index);

        endRemoveRows();
        return true;
    }
    return false;
}

void SerieTableModel::setSerie(Serie* serie)
{
    beginResetModel();
    m_serie = serie;
    endResetModel();
}
