import QtQuick 2.12
import QtQuick.Controls 2.5
import SerieTableModel 1.0
import Serie 1.0

Page
{
    property int score: 0
    width: 700
    height: 420
    title: qsTr("Home")

    Component.onCompleted: {
        nextWord()
        score = 0
    }

    Label
    {
        id: laInitialW
        text: qsTr("mot à deviner")
        font.pointSize: 20
        anchors.verticalCenterOffset: -53
        anchors.horizontalCenterOffset: -5
        anchors.centerIn: parent
    }

    Button
    {
        id: btnNext
        x: 401
        y: 299
        width: 100
        height: 45
        text: qsTr("Ok")
        font.capitalization: Font.Capitalize
        onReleased:
        {
            if (txtInput.text.length > 0)
            {
                correction()
                timer.start()
            }
        }
        Shortcut
        {
            sequence: "Return"
            onActivated: { btnNext.released() }
        }
    }

    Label
    {
        id: laCorrectW
        text: qsTr("correction")
        font.pointSize: 12
        anchors.verticalCenterOffset: 26
        anchors.centerIn: parent
        anchors.horizontalCenterOffset: -5
    }

    Rectangle
    {
        x: 182
        y: 299
        width: 198
        height: 45
        border.color: "black"
        radius: 10
        border.width: 1

        TextInput
        {
            id: txtInput
            x: 5
            y: 5
            width: parent.width - 10
            height: parent.height - 10
            wrapMode: Text.Wrap
            font.pointSize: 12
            onAccepted: btnNext.released()
        }
    }


    Timer
    {
        id: timer
        interval: 1000; running: false; repeat: false
        onTriggered: {
            timer.stop()
            nextWord()
        }
    }

    function nextWord()
    {
        if (serieListModel.currSerie.currIndex < serieListModel.currSerie.shuffleSize)
        {
            serieListModel.currSerie.nextWord()
            laInitialW.text = serieListModel.currSerie.initWord
            laCorrectW.text = serieListModel.currSerie.correctWord
            laCorrectW.visible = false
            txtInput.text = ""
            txtInput.focus = true
        }
        else
        {
            serieListModel.currSerie.score = score
            stackView.push("EndPage.qml")
        }
    }

    function correction()
    {
        laCorrectW.visible = true
        if (laCorrectW.text == txtInput.text)
        {
            laCorrectW.color = "green"
            score++
        }
        else
        {
            laCorrectW.color = "red"
        }
    }
}
