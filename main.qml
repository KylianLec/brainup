import QtQuick 2.12
import QtQuick.Controls 2.5
import Serie 1.0

ApplicationWindow {
    id: root
    width: 700
    height: 420
    visible: true
    title: qsTr("BrainUp")

    StackView {
        id: stackView
        width: 700
        height: 420
        initialItem: "HomePage.qml"
        anchors.fill: parent
    }
}
