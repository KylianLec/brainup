.pragma library

function subs(str, start) {
    return String(str).substr(start, String(str).length);
}
