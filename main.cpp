#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtQml>
#include <QIcon>

#include "SerieListModel.h"
#include "SerieTableModel.h"
#include "Serie.h"

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);
    app.setWindowIcon(QIcon(":/img/res/brain.svg"));

    QQmlApplicationEngine engine;
    qmlRegisterType<Serie>("Serie", 1, 0, "Serie");
    qmlRegisterType<SerieTableModel>("SerieTableModel", 1, 0, "SerieTableModel");

    const QUrl url(QStringLiteral("qrc:/qml/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

    SerieListModel serieListModel;
    serieListModel.loadSeries();
    engine.rootContext()->setContextProperty("serieListModel", &serieListModel);

    SerieTableModel serieTableModel;
    engine.rootContext()->setContextProperty("serieTableModel", &serieTableModel);

    engine.load(url);

    return app.exec();
}
