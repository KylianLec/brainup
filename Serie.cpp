#include "Serie.h"

Serie::Serie()
{}

Serie::Serie(const Serie &serie)
    : m_name(serie.m_name)
{}

Serie::Serie(Serie &&serie)
    : m_name(serie.m_name)
{}

Serie::Serie(const QString &name)
    : m_name(name)
{}

// Shuffle
void Serie::initCurrentList(int nbWord)
{
    m_lstShuffle.clear();
    QVector<int> alreadyUsedIndex;

    while (alreadyUsedIndex.size() < nbWord)
    {
        const unsigned int index = QRandomGenerator::global()->bounded(0, m_lstInit.size());

        if (!alreadyUsedIndex.contains(index))
        {
            m_lstShuffle.append(m_lstInit[index]);
            alreadyUsedIndex.append(index);
        }
    }

    m_currIndex = 0;

    toStringLstShuffle();
}

void Serie::nextWord()
{
    const QPair<QString, QString> curWord = m_lstShuffle[m_currIndex];
    Language nxtWrdLngShown = Language::English;

    switch (m_currMode)
    {
        case Mode::FrEn:
        {
            nxtWrdLngShown = Language::French;
            break;
        }
        case Mode::EnFr:
        {
            nxtWrdLngShown = Language::English;
            break;
        }
        case Mode::Random:
        {
            nxtWrdLngShown = static_cast<Language>(QRandomGenerator::global()->bounded(0, 2));
            break;
        }
    }

    switch (nxtWrdLngShown)
    {
    case Language::English: // show english word
        m_initWord = curWord.first;
        m_correctWord = curWord.second;
        break;
    case Language::French: // show french word
        m_initWord = curWord.second;
        m_correctWord = curWord.first;
        break;
    default:
        qDebug() << "error enum value" << static_cast<int>(nxtWrdLngShown);
        break;
    }

    m_currIndex++;
}

void Serie::toStringLstInit()
{
    for(const auto &p : m_lstInit)
        qDebug() << p.first << p.second;
}

void Serie::toStringLstShuffle()
{
    for(const auto &p : m_lstShuffle)
        qDebug() << p.first << p.second;
}

void Serie::append(const QPair<QString, QString> &newWord)
{
    m_lstInit.append(newWord);
}

void Serie::removeAt(unsigned int index)
{
    m_lstInit.remove(index);
}

Serie &Serie::operator=(const Serie &serie)
{
    m_lstInit = serie.m_lstInit;
    m_lstShuffle = serie.m_lstShuffle;

    m_name = serie.m_name;
    m_currIndex = serie.m_currIndex;
    m_initWord = serie.m_initWord;
    m_correctWord = serie.m_correctWord;
    m_score = serie.m_score;
    m_currMode = serie.m_currMode;

    return (*this);
}

QPair<QString, QString> &Serie::operator[](int index)
{
    return m_lstShuffle[index];
}

QPair<QString, QString> Serie::operator[](int index) const
{
    return m_lstShuffle[index];
}
