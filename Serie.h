#ifndef SERIE_H
#define SERIE_H

#include <QDebug>
#include <QVector>
#include <QPair>
#include <QFile>
#include <QRandomGenerator>

class Serie : public QObject
{
    Q_OBJECT
    Q_ENUMS(Mode)
    Q_PROPERTY(const QString& name READ name WRITE setName)
    Q_PROPERTY(unsigned int shuffleSize READ shuffleSize CONSTANT)
    Q_PROPERTY(unsigned int initSize READ initSize CONSTANT)
    Q_PROPERTY(unsigned int currIndex READ currIndex CONSTANT)
    Q_PROPERTY(const QString& initWord READ initWord CONSTANT)
    Q_PROPERTY(const QString& correctWord READ correctWord CONSTANT)
    Q_PROPERTY(unsigned int score READ score WRITE setScore NOTIFY sigScoreChanged)
    Q_PROPERTY(Mode mode READ mode WRITE setMode NOTIFY sigModeChanged)

public:
    enum class Language
    {
        English = 0,
        French
    };

    enum Mode
    {
        FrEn = 0,
        EnFr,
        Random
        //Double
    };

    Serie();
    Serie(const Serie&);
    Serie(Serie&&);
    Serie(const QString& name);

    Q_INVOKABLE void initCurrentList(int nbWord);
    Q_INVOKABLE void nextWord();
    Q_INVOKABLE void toStringLstInit();
    Q_INVOKABLE void toStringLstShuffle();
    void append(const QPair<QString, QString>&);
    void removeAt(unsigned int index);

    Serie &operator=(const Serie&);
    Q_INVOKABLE QPair<QString, QString>& operator[](int index);      // set (L-value)
    Q_INVOKABLE QPair<QString, QString> operator[](int index) const; // get (R-value)

    /* Getters */
    inline const QString& name() const { return m_name; }
    inline unsigned int initSize() const { return m_lstInit.size(); }
    inline unsigned int shuffleSize() const { return m_lstShuffle.size(); }
    inline unsigned int currIndex() const { return m_currIndex; }
    inline const QString &initWord() const { return m_initWord; }
    inline const QString &correctWord() const { return m_correctWord; }
    inline unsigned int score() const { return m_score; }
    inline Mode mode() const { return m_currMode; }
    inline QPair<QString, QString>& initWord(unsigned int i) { return m_lstInit[i]; }

    /* Setters */
    inline void setName(const QString& name) { m_name = name; }
    inline void setScore(int score) { m_score = score; }
    inline void setMode(int mode) { m_currMode = static_cast<Mode>(mode); }

private:
    // first : english
    // second : french
    QVector<QPair<QString, QString> > m_lstInit;
    QVector<QPair<QString, QString> > m_lstShuffle;

    QString m_name;
    unsigned int m_currIndex;
    QString m_initWord;
    QString m_correctWord;
    unsigned int m_score;
    Mode m_currMode = EnFr;

signals:
    void sigScoreChanged();
    void sigModeChanged();    

};

Q_DECLARE_METATYPE(Serie)

#endif // SERIE_H
