#include "SerieListModel.h"

SerieListModel::SerieListModel(QObject *parent)
    : QAbstractListModel(parent)
{}

int SerieListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_series.size();
}

QVariant SerieListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || m_series.size() <= 0)
        return QVariant();

    switch (role)
    {
    case NameRole:
        return QVariant(m_series[index.row()]->name());
    }

    return QVariant();
}

bool SerieListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (m_series.size() <= 0)
        return false;

    switch (role)
    {
    case NameRole:
        m_series[index.row()]->setName(value.toString());
        break;
    }

    emit dataChanged(index, index, QVector<int>() << role);

    return true;
}

Qt::ItemFlags SerieListModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable;
}

QHash<int, QByteArray> SerieListModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names[NameRole] = "name";
    return names;
}

bool SerieListModel::append()
{
    beginInsertRows(QModelIndex(), m_series.size(), m_series.size());

    const QString nameNewSerie = "Nouvelle série " + QString::number(m_series.size() + 1);
    m_series.append(new Serie(nameNewSerie));

    endInsertRows();
    return true;
}

bool SerieListModel::remove(unsigned int index)
{
    if (index > 0 && index < m_series.size())
    {
        beginRemoveRows(QModelIndex(), index, index);
        m_series.removeAt(index);
        endRemoveRows();
        return true;
    }
    return false;
}

void SerieListModel::loadSeries(const QString& fileName)
{
    QFile file(fileName);

    if (!file.open(QIODevice::ReadOnly))
    {
        qDebug() << "File not open::'"+file.fileName()+"'";
    }

    QByteArray seriesData = file.readAll();

    QJsonDocument loadedSeries(QJsonDocument::fromJson(seriesData));
    const QJsonObject &json = loadedSeries.object();

    if (json.contains("series") && json["series"].isArray())
    {
        QJsonArray arrSeries = json["series"].toArray();
        for (const QJsonValue& value : arrSeries)
        {
            QJsonObject objSerie = value.toObject();
            if (objSerie.contains("name"))
            {
                Serie *s = new Serie(objSerie["name"].toString());
                if (objSerie.contains("words"))
                {
                    QJsonArray arrWords = objSerie["words"].toArray();
                    for (const QJsonValue& value : arrWords)
                    {
                        QJsonObject objWords = value.toObject();
                        QPair<QString, QString> pair;
                        bool en = false;
                        bool fr = false;
                        if (objWords.contains("english"))
                        {
                            pair.first = objWords["english"].toString().toLower();
                            en = true;
                        }
                        else qDebug() << "no english";
                        if (objWords.contains("french"))
                        {
                            pair.second = objWords["french"].toString().toLower();
                            fr = true;
                        }
                        else qDebug() << "no french";
                        if (en && fr) s->append(pair);
                    }
                    beginResetModel();
                    m_series.append(s);
                    endResetModel();
                }
                else qDebug() << "no words";
            }
            else qDebug() << "no name";
        }
    }
    else qDebug() << "no series";
    file.close();
}

void SerieListModel::saveSeries()
{
    QFile file("res/series.json");

    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qDebug() << "File not open '"+file.fileName()+"'";
    }

    QJsonObject json;
    QJsonArray jsonSeries;
    for(int i = 0; i < m_series.size(); ++i)
    {
        QJsonObject jsonSerie;
        jsonSerie["name"] = m_series[i]->name();

        QJsonArray jsonWords;
        for(unsigned int j = 0; j < m_series[i]->initSize(); ++j)
        {
            QJsonObject jsonWord;
            jsonWord["english"] = m_series[i]->initWord(j).first;
            jsonWord["french"] = m_series[i]->initWord(j).second;
            jsonWords.append(jsonWord);

        }
        jsonSerie["words"] = jsonWords;
        jsonSeries.append(jsonSerie);
    }
    json["series"] = jsonSeries;

    QJsonDocument saveDoc(json);
    file.write(saveDoc.toJson());

    file.close();
}

void SerieListModel::setCurrSerie(int i)
{
    if (i != -1) m_currSerie = m_series[i];
}
