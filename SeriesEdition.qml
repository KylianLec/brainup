import QtQuick 2.15
import QtQuick.Controls 2.5
import Serie 1.0
import QtPositioning 5.14
import Qt.labs.qmlmodels 1.0
//import SerieTableModel 1.0
import QtQuick.Dialogs 1.3
import "functions.js" as Func


Page {


    width: 700
    height: 420
    title: qsTr("Home")

    ToolBar {
        id: toolBar
        x: 5
        y: 5
        width: parent.width - 10
        height: 40

        ToolButton {
            id: toolButton
            x: 5
            y: 5
            width: 30
            height: 30
            Image {
                anchors.fill: parent
                source: "qrc:/img/res/house.svg"
            }
            onClicked: {
                serieListModel.saveSeries()
                stackView.pop()
            }
        }

        FileDialog {
            id: fileDialog
            title: "Please choose a file"
            folder: shortcuts.home
            onAccepted: {
                fileDialog.close()
                serieListModel.loadSeries(Func.subs(fileDialog.fileUrls, 8))
            }
            onRejected: {
                fileDialog.close()
            }
        }

        ToolButton {
            id: toolBtnAddSeriesFromJson
            x: 555
            y: 5
            width: 135
            height: 30
            text: qsTr("Ajouter liste via JSON")
            font.pointSize: 9
            font.capitalization: Font.Capitalize
            onClicked: {
                fileDialog.open()
            }
        }
    }

    ButtonGroup { id: rGrpMode }

    Component {
        id: seriesListDelegate
        Item {
            id: itemDelegate
            height: 40
            width: 212
            Rectangle {
                id: rectHightlight
                anchors.fill: parent
                color: "#CBE4FE"
                visible: false
            }
            MouseArea {
                id: itemMouseArea
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    if (listView.currentIndex != index)
                    {
                        listView.currentIndex = index
                        listView.model.setCurrSerie(listView.currentIndex)
                        tableView.model.setSerie(serieListModel.currSerie)
                    }

                }
                onEntered: {
                    rectHightlight.visible = true
                }
                onExited: {
                    rectHightlight.visible = false
                }
            }

            Row {
                id: row
                width: listView.width
                height: 40
                topPadding: 10
                spacing: 10
                Button {
                    id: btnTrash
                    width: 20
                    height: 20
                    background: Rectangle { color: "transparent" }
                    Image {
                        anchors.fill: parent
                        source: "qrc:/img/res/trash.svg"
                    }
                    onClicked: {
                        listView.model.setCurrSerie()
                        tableView.model.setSerie()
                        listView.model.remove(index)
                        if (listView.currentIndex >= listView.model.size())
                            listView.currentIndex -= 1
                    }
                }

                TextInput {
                    id: serieName
                    text: model.name
                    font.bold: true
                    onEditingFinished: {
                        model.name = text
                    }
                    onFocusChanged: {
                        if (serieName.focus == true)
                            itemMouseArea.clicked(Qt.LeftButton)
                    }
                }
            }
        }
    }

    ListView {
        id: listView
        x: 5
        y: toolBar.y + toolBar.height + 5
        width: 212
        height: 330
        leftMargin: 0
        rightMargin: 0
        bottomMargin: 0
        topMargin: 0
        delegate: seriesListDelegate
        model: serieListModel
        currentIndex: -1
        clip: true
        highlight: Rectangle
        {
            color: "#72CCFE"
            opacity: 0.7
            focus: true
            Behavior on y {
                SpringAnimation {
                    spring: 3
                    damping: 0.2
                }
            }
        }
    }

    Button {
        id: btnAddSerie
        x: 5
        y: parent.height - height - 5
        width: 212
        height: 35
        text: qsTr("Nouveau")
        font.capitalization: Font.Capitalize
        onClicked: {
            listView.model.append()
        }
    }

    Component {
        id: tableSerieDelegate
        Rectangle {
            id: cellTableDelegate
            color: (tableView.currentRow === row) ? "#72CCFE" : (row%2 == 0) ? "white" : "#CEDAED"
            MouseArea {
                id: cellMouseAre
                anchors.fill: parent
                onClicked: {
                    if (tableView.currentRow !== row && row !== 0)
                    {
                        tableView.currentRow = row
                        focus = true
                    }
                    else
                        delegateText.deselect()
                }
                onDoubleClicked: {
                    delegateText.selectAll()
                }
            }
            TextInput {
                id: delegateText
                text: model.display
                anchors.centerIn: parent
                wrapMode: Text.WrapAnywhere
                font.pointSize: 10
                onEditingFinished: {
                    model.display = text
                }
            }
        }
    }

    TableView {
        id: tableView
        x: listView.x + listView.width + 5
        y: toolBar.y + toolBar.height + 5
        width: 420
        height: parent.height - y - 5
        model: serieTableModel
        clip: true
        property var currentRow: -1
        property var rowHeight: [25, 50]
        rowHeightProvider: function (row) { return row === 0 ? rowHeight[0] : rowHeight[1] }
        columnWidthProvider: function (col) { return width/2 }
        delegate: tableSerieDelegate

    }

    Column {
        id: colWordEdition
        y: tableView.y
        width: parent.width - x - 5
        height: tableView.height
        spacing: 10
        property var btnWidth: width - 5
        property var btnHeight: 30
        property var btnX: 5
        x: 642
        Button {
            x: parent.btnX
            id: btnAddWord
            width: parent.btnWidth
            height: parent.btnHeight
            text: qsTr("+")
            font.capitalization: Font.Capitalize
            onClicked: {
                tableView.model.append()
            }
        }
        Button {
            x: parent.btnX
            id: btnRemoveWord
            width: parent.btnWidth
            height: parent.btnHeight
            text: qsTr("-")
            font.capitalization: Font.Capitalize
            onClicked: {
                if (tableView.currentRow !== -1)
                {
                    tableView.model.remove(tableView.currentRow)
                    tableView.currentRow = -1
                }
            }
        }
    }

    Rectangle {
        id: rectangle
        x: 219
        y: toolBar.y + toolBar.height + 5
        width: 1
        height: parent.height - 10 - y
        color: "#c8c8c8"
    }

    Rectangle {
        id: rectangle1
        x: 640
        y: toolBar.y + toolBar.height + 5
        width: 1
        height: parent.height - 10 - y
        color: "#c8c8c8"
    }
}

/*##^##
Designer {
    D{i:0;formeditorZoom:1.75}D{i:27}D{i:28}
}
##^##*/
