#ifndef SERIETABLEVIEW_H
#define SERIETABLEVIEW_H

#include <QObject>
#include <QAbstractTableModel>

#include "Serie.h"

class SerieTableModel : public QAbstractTableModel
{
    Q_OBJECT
    enum SerieTableRole {
        DataRole = Qt::UserRole + 1,
    };

public:
    SerieTableModel();

    virtual int rowCount(const QModelIndex &parent) const override;
    virtual int columnCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role) override;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    virtual QHash<int, QByteArray> roleNames() const override;
    virtual Qt::ItemFlags flags(const QModelIndex &index) const override;

    Q_INVOKABLE bool append();
    Q_INVOKABLE bool remove(unsigned int index);

    Q_INVOKABLE void setSerie(Serie* serie = nullptr);

private:
    Serie *m_serie = nullptr;
};

#endif // SERIETABLEVIEW_H
